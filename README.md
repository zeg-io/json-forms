# JSON Forms

A module for dynamically creating web forms based on JSON data.

## Installation

`npm i -S json-forms`

## Usage

```
var iForms = require(json-forms);

```