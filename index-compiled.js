"use strict";

/**
 * Created by tonyarcher on 9/2/16.
 */

var buildForm = function buildForm(formJson) {

  var fields = formJson.fields,
      params = {};

  var buildField = function buildField(name, options) {
    console.log(name + " -> " + JSON.stringify(options));
  };

  // TODO: Use Handlebars to build html?

  for (var i = 0; i < fields.length; i++) {
    for (var fieldName in fields[i]) {
      if (fields[i].hasOwnProperty(fieldName)) {

        buildField(fieldName, fields[i][fieldName]);
      }
    }
  }
};

module.exports = buildForm;

//# sourceMappingURL=index-compiled.js.map