/**
 * Created by tonyarcher on 9/2/16.
 */

let buildForm = (formJson) => {

  let fields = formJson.fields,
      params = {}

  let buildField = (name, options) => {
    console.log(`${name} -> ${JSON.stringify(options)}`)
  }

  // TODO: Use Handlebars to build html?

  for (let i = 0; i < fields.length; i++) {
    for (let fieldName in fields[i]) {
      if (fields[i].hasOwnProperty(fieldName)) {

        buildField(fieldName, fields[i][fieldName])
      }
    }
  }
}

module.exports = buildForm